package com.dzhjupiter.customer_lending.controller;

import com.dzhjupiter.customer_lending.CustomerLendingApplicationTests;
import com.dzhjupiter.customer_lending.Utils.AppUtils;
import com.dzhjupiter.customer_lending.restModel.ClientRq;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

import static com.dzhjupiter.customer_lending.controller.CreditControllerTest.fillCreditData;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClientControllerTest extends CustomerLendingApplicationTests {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    /**
     * замена индивидуального лимита для пользователя
     */
    @Test
    public void changeLimit() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/credits/create")
                .content(AppUtils.convertObjectToJson(fillCreditData()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.creditStatus").value("APPROVED - Заявка успешно прошла проверку"));

        ClientRq clientRq = new ClientRq();
        clientRq.setPersonalId("123456-78");
        clientRq.setPersonalLimit(new BigDecimal(2000));
        mockMvc.perform(MockMvcRequestBuilders.put("/clients/change-limit")
                .content(AppUtils.convertObjectToJson(clientRq))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("Индивидуальный лимит успешно изменен на 2000"));
    }
}
