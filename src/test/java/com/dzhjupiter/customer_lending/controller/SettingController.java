package com.dzhjupiter.customer_lending.controller;

import com.dzhjupiter.customer_lending.service.SettingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@WebMvcTest(value = SettingController.class)
public class SettingController {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SettingService settingService;
}
