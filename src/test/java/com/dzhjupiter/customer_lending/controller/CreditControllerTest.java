package com.dzhjupiter.customer_lending.controller;

import com.dzhjupiter.customer_lending.CustomerLendingApplicationTests;
import com.dzhjupiter.customer_lending.Utils.AppUtils;
import com.dzhjupiter.customer_lending.enums.CreditStatusEnum;
import com.dzhjupiter.customer_lending.restModel.CreditShortRq;
import com.dzhjupiter.customer_lending.restModel.CreditFilterRq;
import com.dzhjupiter.customer_lending.restModel.CreditRq;
import com.dzhjupiter.customer_lending.restModel.CreditShortResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CreditControllerTest extends CustomerLendingApplicationTests {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    static CreditRq fillCreditData() {
        CreditRq restModel = new CreditRq();
        restModel.setFirstName("TestFirstName");
        restModel.setLastName("TestLastName");
        restModel.setAmount(new BigDecimal(100));
        restModel.setDuration(5);
        restModel.setPersonalId("123456-78");

        return restModel;
    }

    /**
     * подача заявки на кредит (сумма кредита, срок, имя, фамилия, личный идентификатор, страна)
     */
    @Test
    public void createCredit() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/credits/create")
                .content(AppUtils.convertObjectToJson(fillCreditData()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.creditStatus").value("APPROVED - Заявка успешно прошла проверку"));
    }

    /**
     * погашение кредита (идентификатор кредита, личный идентификатор)
     */
    @Test
    public void repayCredit() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/credits/create")
                .content(AppUtils.convertObjectToJson(fillCreditData()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.creditStatus", is("APPROVED - Заявка успешно прошла проверку")));
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        CreditShortResponse response = objectMapper.readValue(contentAsString, CreditShortResponse.class);

        CreditShortRq claimRq = new CreditShortRq();
        claimRq.setCreditId(response.getCreditId());
        claimRq.setPersonalId("123456-78");
        mockMvc.perform(MockMvcRequestBuilders.post("/credits/repay")
                .content(AppUtils.convertObjectToJson(claimRq))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string("Кредит погашен"));
    }

    /**
     * переподача ранее отклонённой заявки (идентификатор кредита, личный идентификатор)
     */
    @Test
    public void reSubmitCredit() throws Exception {
        CreditRq restModel = fillCreditData();
        restModel.setAmount(new BigDecimal(1200.00));
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/credits/create")
                .content(AppUtils.convertObjectToJson(restModel))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.creditStatus").value("REJECTED - Превышен лимит по сумме непогашенных кредитов '1000.00' у пользователя (по личному идентификатору '123456-78')"));

        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        CreditShortResponse response = objectMapper.readValue(contentAsString, CreditShortResponse.class);

        CreditShortRq claimRq = new CreditShortRq();
        claimRq.setCreditId(response.getCreditId());
        claimRq.setPersonalId("123456-78");
        mockMvc.perform(MockMvcRequestBuilders.post("/credits/resubmit")
                .content(AppUtils.convertObjectToJson(claimRq))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * Список всех кредитов (с возможностью фильтрации по статусам (Утверждён, Отклонён, Погашен) и по пользователю (личный идентификатор))
     */
    @Test
    public void getAllCreditsWithFilter() throws Exception {
        CreditFilterRq filter = new CreditFilterRq();
        filter.setPersonalId(null);
        filter.setStatus(CreditStatusEnum.APPROVED.toString());
        mockMvc.perform(MockMvcRequestBuilders.put("/credits/filter")
                .content(AppUtils.convertObjectToJson(filter))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].amount", is(100.00)))
                .andExpect(jsonPath("$.content[0].firstName", is("TestFirstName")))
                .andExpect(jsonPath("$.content[0].personalId", is("123456-78")));
    }
}
