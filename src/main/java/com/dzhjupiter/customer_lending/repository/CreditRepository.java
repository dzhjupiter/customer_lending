package com.dzhjupiter.customer_lending.repository;

import com.dzhjupiter.customer_lending.entity.CreditEntity;
import com.dzhjupiter.customer_lending.enums.CreditStatusEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface CreditRepository extends JpaRepository<CreditEntity, Long> {

    @Query(value = "FROM CreditEntity crdt WHERE " +
            "LOWER(crdt.personalId) = LOWER(:personalId) AND " +
            "LOWER(crdt.uuid) = LOWER(:creditId) AND " +
            "LOWER(crdt.creditState) = LOWER(:rejected) ")
    CreditEntity findByUuidAndPersonalIdAndStatus(String creditId, String personalId, String rejected);

    @Query(value = "FROM CreditEntity crdt WHERE " +
            "LOWER(crdt.personalId) = LOWER(:personalId) AND " +
            "LOWER(crdt.uuid) = LOWER(:creditId)")
    CreditEntity findByUuidAndPersonalId(String creditId, String personalId);

    @Query(value = "SELECT crdt FROM CreditEntity crdt WHERE " +
            "(:personalId is null or LOWER(crdt.personalId) = LOWER(:personalId)) AND " +
            "(:status is null or LOWER(crdt.creditState) = LOWER(:status))")
    Page<CreditEntity> findAll(String personalId, String status, Pageable pageable);

    @Query(value = "SELECT COUNT(crdt) FROM CreditEntity crdt WHERE " +
            "LOWER(crdt.country) = LOWER(:country) AND crdt.creationDate >= :time ")
    Long countActualCreditsByCountry(String country, LocalDateTime time);

    @Query(value = "SELECT SUM(crdt.amount) FROM CreditEntity crdt WHERE " +
            "LOWER(crdt.personalId) = LOWER(:personalId) AND " +
            "LOWER(crdt.creditState) = LOWER(:approved) ")
    BigDecimal sumActualAmountByPersonalId(String personalId, String approved);
}
