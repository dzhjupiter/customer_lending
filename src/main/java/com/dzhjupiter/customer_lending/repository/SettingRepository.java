package com.dzhjupiter.customer_lending.repository;

import com.dzhjupiter.customer_lending.entity.SettingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SettingRepository extends JpaRepository<SettingEntity, Long> {
    SettingEntity findByKey(String key);
}
