package com.dzhjupiter.customer_lending.repository;


import com.dzhjupiter.customer_lending.entity.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {

    ClientEntity findByPersonalId(String personalId);

    Boolean existsByPersonalId(String personalIdClient);
}
