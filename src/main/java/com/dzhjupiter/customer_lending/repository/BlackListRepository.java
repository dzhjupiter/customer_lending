package com.dzhjupiter.customer_lending.repository;

import com.dzhjupiter.customer_lending.entity.BlackListEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlackListRepository extends JpaRepository<BlackListEntity, Long> {

    Boolean existsByPersonalIdClient(String personalIdClient);
}
