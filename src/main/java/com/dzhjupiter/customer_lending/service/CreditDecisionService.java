package com.dzhjupiter.customer_lending.service;

import com.dzhjupiter.customer_lending.entity.ClientEntity;
import com.dzhjupiter.customer_lending.entity.CreditEntity;
import com.dzhjupiter.customer_lending.enums.CreditStatusEnum;
import com.dzhjupiter.customer_lending.repository.BlackListRepository;
import com.dzhjupiter.customer_lending.repository.ClientRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.xml.bind.ValidationException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Service
public class CreditDecisionService {

    private static final Logger LOGGER = Logger.getLogger(CreditDecisionService.class);

    @Autowired
    CreditService creditService;
    @Autowired
    SettingService settingService;
    @Autowired
    BlackListService blackListService;
    @Autowired
    BlackListRepository blackListRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    ClientService clientService;


    @Transactional
    public CreditEntity makeDecision(CreditEntity convertedEntity) {
        try {
            checkBlackList(convertedEntity);
            checkLimitsForCountry(convertedEntity);
            checkLimitAmountForClient(convertedEntity);
        } catch (ValidationException ex) {
            LOGGER.warn(ex);
            convertedEntity.setCreditState(CreditStatusEnum.REJECTED);
            convertedEntity.setDecisionDesc(ex.getMessage());
            return creditService.save(convertedEntity);
        }
        convertedEntity.setCreditState(CreditStatusEnum.APPROVED);
        convertedEntity.setDecisionDesc("Заявка успешно прошла проверку");
        if(clientRepository.existsByPersonalId(convertedEntity.getPersonalId())){
            return creditService.save(convertedEntity);
        }
        clientService.createUser(convertedEntity);
        return creditService.save(convertedEntity);
    }

    private void checkLimitAmountForClient(CreditEntity convertedEntity) throws ValidationException {
        ClientEntity client = clientRepository.findByPersonalId(convertedEntity.getPersonalId());
        BigDecimal limitAmount;
        if(Objects.isNull(client)){
            limitAmount = settingService.getLimitAmountForUsers(null);
        } else {
            limitAmount = client.getPersonalLimit();
        }
        BigDecimal actualAmount = creditService.sumActualAmount(convertedEntity.getPersonalId());
        actualAmount = actualAmount.add(convertedEntity.getAmount());
        if(actualAmount.compareTo(limitAmount) > 0){
            throw new ValidationException(String.format("Превышен лимит по сумме непогашенных кредитов '%s' у " +
                            "пользователя (по личному идентификатору '%s')", limitAmount,convertedEntity.getPersonalId()));
        }
    }

    private void checkLimitsForCountry(CreditEntity entity) throws ValidationException {
        String country = StringUtils.isNotBlank(entity.getCountry()) ? entity.getCountry() : StringUtils.EMPTY;
        long countLimitCredits = settingService.getCountLimitCredits(country);
        long limitMinutes = settingService.getLimitMinutes(country);
        long actualCount = creditService.findActualCountCreditsByCountry(country,
                LocalDateTime.now().minusSeconds(limitMinutes * 60));

        if (actualCount > countLimitCredits) {
            throw new ValidationException(String.format("Для страны '%s' превышен лимит '%s' заявок в '%s' минут.",
                    country, countLimitCredits, limitMinutes));
        }

    }

    private void checkBlackList(CreditEntity convertedEntity) throws ValidationException {
        Boolean isExists = blackListRepository.existsByPersonalIdClient(convertedEntity.getPersonalId());
        if (Boolean.TRUE.equals(isExists)) {
            throw new ValidationException("Пользователь находится в черном списке. Выдача кредита невозможна");
        }
    }
}
