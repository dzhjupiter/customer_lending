package com.dzhjupiter.customer_lending.service;

import com.dzhjupiter.customer_lending.entity.ClientEntity;
import com.dzhjupiter.customer_lending.entity.CreditEntity;
import com.dzhjupiter.customer_lending.repository.ClientRepository;
import com.dzhjupiter.customer_lending.restModel.ClientRq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.Objects;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    public void createUser(CreditEntity convertedEntity) {
        ClientEntity entity = new ClientEntity();
        entity.setFirstName(convertedEntity.getFirstName());
        entity.setLastName(convertedEntity.getLastName());
        entity.setPersonalId(convertedEntity.getPersonalId());
        entity.setPersonalLimit(new BigDecimal(1000));

        clientRepository.save(entity);
    }

    public ClientEntity changeLimit(ClientRq client) {
        ClientEntity clientEntity = clientRepository.findByPersonalId(client.getPersonalId());
        if(Objects.nonNull(clientEntity)){
            clientEntity.setPersonalLimit(client.getPersonalLimit());
            return clientRepository.save(clientEntity);
        }
        throw new EntityNotFoundException();
    }
}
