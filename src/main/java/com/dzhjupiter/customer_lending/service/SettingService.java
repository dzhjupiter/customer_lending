package com.dzhjupiter.customer_lending.service;

import com.dzhjupiter.customer_lending.entity.SettingEntity;
import com.dzhjupiter.customer_lending.repository.SettingRepository;
import com.dzhjupiter.customer_lending.restModel.SettingsData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.Objects;

@Service
public class SettingService {

    private static final String LIMIT_COUNT = "count_limit_";
    private static final String LIMIT_MINUTES = "count_minutes_";
    private static final String DEFAULT_LIMIT_COUNT = "5";
    private static final String DEFAULT_LIMIT_MINUTES = "3";
    private static final String DEFAULT_AMOUNT_LIMIT_MINUTES = "1000.00";

    @Autowired
    SettingRepository settingRepository;

    public SettingEntity getSetting(String key) {
        SettingEntity settingEntity = settingRepository.findByKey(key);
        if (Objects.nonNull(settingEntity)) {
            return settingEntity;
        }
        throw new EntityNotFoundException();
    }

    public SettingEntity editSetting(SettingsData data) {
        SettingEntity entity = getSetting(data.getKey());
        entity.setValue(data.getValue());
        return settingRepository.save(entity);
    }

    public long getCountLimitCredits(String country) {
        String value = getValueByKeyOrDefault(LIMIT_COUNT.concat(country.trim().toLowerCase()), DEFAULT_LIMIT_COUNT);
        return Long.parseLong(value);
    }

    public long getLimitMinutes(String country) {
        String value = getValueByKeyOrDefault(LIMIT_MINUTES.concat(country.trim().toLowerCase()), DEFAULT_LIMIT_MINUTES);
        return Long.parseLong(value);
    }

    public BigDecimal getLimitAmountForUsers(String personalId) {
        String value = getValueByKeyOrDefault(personalId, DEFAULT_AMOUNT_LIMIT_MINUTES);
        return new BigDecimal(value);
    }

    private String getValueByKeyOrDefault(String key, String defaultValue) {
        SettingEntity settingEntity = settingRepository.findByKey(key);
        if (Objects.isNull(settingEntity) || StringUtils.isEmpty(settingEntity.getValue())) {
            return defaultValue;
        }
        return settingEntity.getValue();
    }

}
