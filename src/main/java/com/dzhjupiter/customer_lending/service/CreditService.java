package com.dzhjupiter.customer_lending.service;

import com.dzhjupiter.customer_lending.entity.CreditEntity;
import com.dzhjupiter.customer_lending.enums.CreditStatusEnum;
import com.dzhjupiter.customer_lending.repository.CreditRepository;
import com.dzhjupiter.customer_lending.restModel.CreditFilterRq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

@Service
public class CreditService {

    @Autowired
    CreditRepository creditRepository;

    CreditEntity save(CreditEntity loanEntity) {
        return creditRepository.save(loanEntity);
    }

    public CreditEntity repayCredit(String creditId, String personalId) {
        CreditEntity creditEntity = getCreditByCreditIdAndPersonalId(creditId, personalId);
        creditEntity.setCreditState(CreditStatusEnum.REPAID);
        return creditRepository.save(creditEntity);
    }

    public CreditEntity getCreditByCreditIdAndPersonalId(String creditId, String personalId) {
        CreditEntity entity = creditRepository.findByUuidAndPersonalId(creditId, personalId);
        if(Objects.nonNull(entity)){
            return entity;
        }
        throw new EntityNotFoundException();
    }
    public CreditEntity getCreditByCreditIdAndPersonalIdAndStatus(String creditId, String personalId) {
        CreditEntity entity = creditRepository.findByUuidAndPersonalIdAndStatus(creditId, personalId, CreditStatusEnum.REJECTED.toString());
        if(Objects.nonNull(entity)){
            return entity;
        }
        throw new EntityNotFoundException();
    }

    public Page<CreditEntity> findAll(Pageable pageable) {
        return creditRepository.findAll(pageable);
    }

    public Page<CreditEntity> findAllByFilter(CreditFilterRq filter, Pageable pageable) {
        return creditRepository.findAll(filter.getPersonalId(),filter.getStatus(),pageable);
    }

    public long findActualCountCreditsByCountry(String country, LocalDateTime time) {
        return creditRepository.countActualCreditsByCountry(country,time);
    }

    public BigDecimal sumActualAmount(String personalId) {
        return Optional.ofNullable(creditRepository.sumActualAmountByPersonalId(personalId,CreditStatusEnum.APPROVED.toString())).orElse(BigDecimal.ZERO);
    }
}
