package com.dzhjupiter.customer_lending;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerLendingApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomerLendingApplication.class, args);
    }

}
