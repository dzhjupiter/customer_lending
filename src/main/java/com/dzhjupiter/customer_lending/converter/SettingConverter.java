package com.dzhjupiter.customer_lending.converter;

import com.dzhjupiter.customer_lending.entity.SettingEntity;
import com.dzhjupiter.customer_lending.restModel.SettingsData;

public class SettingConverter {
    public static SettingsData convert(SettingEntity settingEntity) {
        SettingsData settingData = new SettingsData();
        settingData.setDescription(settingEntity.getDescription());
        settingData.setKey(settingEntity.getKey());
        settingData.setValue(settingEntity.getValue());
        return settingData;
    }

    public static SettingEntity convert(SettingsData settingData) {
        SettingEntity settingEntity = new SettingEntity();
        settingEntity.setDescription(settingData.getDescription());
        settingEntity.setKey(settingData.getKey());
        settingEntity.setValue(settingData.getValue());
        return settingEntity;
    }
}
