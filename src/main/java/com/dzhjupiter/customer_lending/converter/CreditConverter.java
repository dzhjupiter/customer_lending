package com.dzhjupiter.customer_lending.converter;

import com.dzhjupiter.customer_lending.entity.CreditEntity;
import com.dzhjupiter.customer_lending.restModel.CreditRq;

public class CreditConverter {

    public static CreditEntity convert(CreditRq restModel) {
        CreditEntity creditEntity = new CreditEntity();

        creditEntity.setFirstName(restModel.getFirstName());
        creditEntity.setLastName(restModel.getLastName());
        creditEntity.setAmount(restModel.getAmount());
        creditEntity.setDuration(restModel.getDuration());
        creditEntity.setCountry(restModel.getCountry());
        creditEntity.setPersonalId(restModel.getPersonalId());

        return creditEntity;
    }

    public static CreditEntity convert(CreditEntity creditEntity) {
        CreditRq restModel = new CreditRq();

        restModel.setFirstName(creditEntity.getFirstName());
        restModel.setLastName(creditEntity.getLastName());
        restModel.setAmount(creditEntity.getAmount());
        restModel.setDuration(creditEntity.getDuration());
        restModel.setCountry(creditEntity.getCountry());
        restModel.setPersonalId(creditEntity.getPersonalId());

        return creditEntity;
    }
}

