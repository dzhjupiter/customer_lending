package com.dzhjupiter.customer_lending.controller;


import com.dzhjupiter.customer_lending.converter.SettingConverter;
import com.dzhjupiter.customer_lending.entity.SettingEntity;
import com.dzhjupiter.customer_lending.restModel.SettingsData;
import com.dzhjupiter.customer_lending.service.SettingService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Objects;

@RestController
@RequestMapping("/settings")
public class SettingsController {

    private static final Logger LOGGER = Logger.getLogger(SettingsController.class);

    @Autowired
    SettingService settingService;

    @RequestMapping(params = {"key"}, method = RequestMethod.GET)
    public ResponseEntity<?> getSettingByKey(@RequestParam(value = "key") String key) {
        try {
            SettingEntity settingEntity = settingService.getSetting(key);
            return ResponseEntity.ok().body(SettingConverter.convert(settingEntity));
        } catch (EntityNotFoundException e) {
            LOGGER.warn(e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Параметр с таким идентификатором не найден");
        }
    }

    @RequestMapping(consumes = "application/json", produces = "application/json", value = "change", method = RequestMethod.PUT)
    public ResponseEntity<?> changeValue(@RequestBody SettingsData settingsData) {
        try{
            settingService.editSetting(settingsData);
            return ResponseEntity.ok().body(String.format("Значение параметра успешно изменено на %s",settingsData.getValue()));
        } catch (EntityNotFoundException e) {
            LOGGER.warn(e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Параметр с таким идентификатором не найден");
        }

    }

}
