package com.dzhjupiter.customer_lending.controller;

import com.dzhjupiter.customer_lending.Utils.AppUtils;
import com.dzhjupiter.customer_lending.restModel.ClientRq;
import com.dzhjupiter.customer_lending.service.ClientService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;

@RestController
@RequestMapping("/clients")
public class ClientController {

    private static final Logger LOGGER = Logger.getLogger(ClientController.class);

    @Autowired
    ClientService clientService;

    @RequestMapping(consumes = "application/json", produces = "application/json", value = "change-limit", method = RequestMethod.PUT)
    public ResponseEntity<?> changeLimit(@RequestBody ClientRq clientRq) {
        if (AppUtils.isBlankFields(clientRq.getPersonalId(), String.valueOf(clientRq.getPersonalLimit()))) {
            return ResponseEntity.badRequest().body("Не заполнено одно из обязательных полей");
        }
        try {
            clientService.changeLimit(clientRq);
        } catch (EntityNotFoundException e) {
            LOGGER.warn(e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Пользователь не найден");
        }
        return ResponseEntity.ok().body(String.format("Индивидуальный лимит успешно изменен на %s", clientRq.getPersonalLimit()));
    }
}
