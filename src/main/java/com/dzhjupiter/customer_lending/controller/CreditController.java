package com.dzhjupiter.customer_lending.controller;

import com.dzhjupiter.customer_lending.Utils.AppUtils;
import com.dzhjupiter.customer_lending.Utils.CreditValidator;
import com.dzhjupiter.customer_lending.converter.CreditConverter;
import com.dzhjupiter.customer_lending.entity.CreditEntity;
import com.dzhjupiter.customer_lending.enums.CreditStatusEnum;
import com.dzhjupiter.customer_lending.restModel.CreditShortRq;
import com.dzhjupiter.customer_lending.restModel.CreditFilterRq;
import com.dzhjupiter.customer_lending.restModel.CreditRq;
import com.dzhjupiter.customer_lending.restModel.CreditShortResponse;
import com.dzhjupiter.customer_lending.service.CreditDecisionService;
import com.dzhjupiter.customer_lending.service.CreditService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import javax.xml.bind.ValidationException;
import java.util.Objects;


@RestController
@RequestMapping("/credits")
public class CreditController {

    private static final Logger LOGGER = Logger.getLogger(CreditController.class);

    @Autowired
    CreditService creditService;
    @Autowired
    CreditDecisionService creditDecisionService;

    /**
     * Создать кредит
     */
    @RequestMapping(consumes = "application/json", produces = "application/json", value = "create", method = RequestMethod.POST)
    public ResponseEntity<?> createCredit(@RequestBody CreditRq restModel) {
        CreditEntity convertedEntity = CreditConverter.convert(restModel);
        try {
            CreditValidator.validate(convertedEntity);
        } catch (ValidationException e) {
            LOGGER.warn(e);
            return ResponseEntity.badRequest().body(new CreditShortResponse(null, e.getMessage()));
        }
        CreditEntity credit = creditDecisionService.makeDecision(convertedEntity);
        return ResponseEntity.ok().body(new CreditShortResponse(credit.getUuid(),
                String.format("%s - %s", credit.getCreditState().name(), credit.getDecisionDesc())));
    }

    /**
     * Погасить кредит
     */
    @RequestMapping(produces = "text/plain;charset=UTF-8", value = "repay", method = RequestMethod.POST)
    public ResponseEntity<?> repayCredit(@RequestBody CreditShortRq creditShortRq) {
        if (AppUtils.isBlankFields(creditShortRq.getCreditId(), creditShortRq.getPersonalId())) {
            return ResponseEntity.badRequest().body("Не заполнено одно из обязательных полей");
        }
        CreditEntity creditEntity;
        try {
            creditEntity = creditService.repayCredit(creditShortRq.getCreditId(), creditShortRq.getPersonalId());
            if (Objects.isNull(creditEntity) || !CreditStatusEnum.REPAID.equals(creditEntity.getCreditState())) {
                String errorMessage = String.format("Произошла ошибка при погашении кредита %s", creditShortRq.getCreditId());
                LOGGER.error(errorMessage);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
            }
        } catch (EntityNotFoundException e) {
            LOGGER.warn(e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Кредит c такими параметрами не найден");
        }
        return ResponseEntity.ok("Кредит погашен");
    }

    /**
     * Переподать ранее отклонённую заявку
     */
    @RequestMapping(consumes = "application/json", produces = "application/json", value = "resubmit", method = RequestMethod.POST)
    public ResponseEntity<?> reSubmitCredit(@RequestBody CreditShortRq creditShortRq) {
        if (AppUtils.isBlankFields(creditShortRq.getCreditId(), creditShortRq.getPersonalId())) {
            return ResponseEntity.badRequest().body("Не заполнено одно из обязательных полей");
        }
        CreditEntity entity;
        try {
            entity = creditService.getCreditByCreditIdAndPersonalIdAndStatus(creditShortRq.getCreditId(), creditShortRq.getPersonalId());
            CreditValidator.validate(entity);
        } catch (EntityNotFoundException e) {
            LOGGER.warn(e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CreditShortResponse(null,
                    "По заданным параметрам отклонненный кредит не найден"));
        } catch (ValidationException ex) {
            LOGGER.warn(ex);
            return ResponseEntity.badRequest().body(new CreditShortResponse(null, ex.getMessage()));
        }
        creditDecisionService.makeDecision(entity);
        return ResponseEntity.ok().body(new CreditShortResponse(entity.getUuid(),
                String.format("%s - %s", entity.getCreditState().name(), entity.getDecisionDesc())));
    }

    @RequestMapping(produces = "application/json", value = "filter", method = RequestMethod.PUT)
    public ResponseEntity<?> getAllCreditsByFilter(@PageableDefault(value = 50) Pageable pageable,
                                                   @RequestBody(required = false) CreditFilterRq filter) {
        if (Objects.isNull(filter) || AppUtils.isBlankFields(filter.getStatus(), filter.getPersonalId())) {
            return ResponseEntity.ok().body(creditService.findAll(pageable).map(CreditConverter::convert));
        }
        return ResponseEntity.ok().body(creditService.findAllByFilter(filter, pageable).map(CreditConverter::convert));
    }
}
