package com.dzhjupiter.customer_lending.entity;


import javax.persistence.*;

@Entity
@Table(name = "black_list")
public class BlackListEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String personalIdClient;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPersonalIdClient() {
        return personalIdClient;
    }

    public void setPersonalIdClient(String personalIdClient) {
        this.personalIdClient = personalIdClient;
    }
}
