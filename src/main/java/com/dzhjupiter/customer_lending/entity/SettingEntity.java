package com.dzhjupiter.customer_lending.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "settings")
public class SettingEntity {

    @Id
    private String key;
    private String value;
    private String description;

    public SettingEntity(String key, String value, String description) {
        this.key = key;
        this.value = value;
        this.description = description;
    }

    public SettingEntity() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
