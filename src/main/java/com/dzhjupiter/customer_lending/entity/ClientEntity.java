package com.dzhjupiter.customer_lending.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "client")
public class ClientEntity {

    @Id
    private int id;
    private String firstName;
    private String lastName;
    private String personalId;

    private BigDecimal personalLimit;

    public ClientEntity(int id, String firstName, String lastName, String personalId, BigDecimal personalLimit) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.personalId = personalId;
        this.personalLimit = personalLimit;
    }

    public ClientEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public BigDecimal getPersonalLimit() {
        return personalLimit;
    }

    public void setPersonalLimit(BigDecimal personalLimit) {
        this.personalLimit = personalLimit;
    }
}
