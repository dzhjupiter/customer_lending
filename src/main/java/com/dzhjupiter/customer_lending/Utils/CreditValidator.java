package com.dzhjupiter.customer_lending.Utils;

import com.dzhjupiter.customer_lending.entity.CreditEntity;

import javax.xml.bind.ValidationException;
import java.math.BigDecimal;
import java.util.Objects;

public class CreditValidator {
    public static void validate(CreditEntity creditEntity) throws ValidationException {
        if(Objects.isNull(creditEntity)){
            throw new ValidationException("Получен пустой объект завки на кредит");
        }
        if(Objects.nonNull(creditEntity.getAmount()) && creditEntity.getAmount().compareTo(BigDecimal.ZERO) < 1){
            throw new ValidationException("Сумма кредита должна быть больше нуля");
        }
    }
}
