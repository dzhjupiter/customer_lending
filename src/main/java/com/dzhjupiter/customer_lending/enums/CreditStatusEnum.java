package com.dzhjupiter.customer_lending.enums;

public enum CreditStatusEnum {
    APPROVED,
    REJECTED,
    REPAID
}
