package com.dzhjupiter.customer_lending.restModel;

public class CreditShortResponse {

    private String creditId;
    private String creditStatus;

    public CreditShortResponse(String creditId, String creditStatus) {
        this.creditId = creditId;
        this.creditStatus = creditStatus;
    }

    public String getCreditId() {
        return creditId;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }

    public String getCreditStatus() {
        return creditStatus;
    }

    public void setCreditStatus(String creditStatus) {
        this.creditStatus = creditStatus;
    }

    @Override
    public String toString() {
        return "CreditShortResponse{" +
                "creditId='" + creditId + '\'' +
                ", creditStatus='" + creditStatus + '\'' +
                '}';
    }
}
