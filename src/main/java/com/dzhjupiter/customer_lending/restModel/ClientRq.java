package com.dzhjupiter.customer_lending.restModel;

import java.math.BigDecimal;

public class ClientRq {

    private String personalId;
    private BigDecimal personalLimit;

    public BigDecimal getPersonalLimit() {
        return personalLimit;
    }

    public void setPersonalLimit(BigDecimal personalLimit) {
        this.personalLimit = personalLimit;
    }


    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }
}
