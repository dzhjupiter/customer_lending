package com.dzhjupiter.customer_lending.restModel;

public class CreditShortRq {
    private String creditId;
    private String personalId;

    public CreditShortRq() {
    }

    public String getCreditId() {
        return creditId;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }
}
