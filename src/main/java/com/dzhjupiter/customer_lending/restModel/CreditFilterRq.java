package com.dzhjupiter.customer_lending.restModel;

public class CreditFilterRq {
    private String status;
    private String personalId;

    public CreditFilterRq() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }
}
